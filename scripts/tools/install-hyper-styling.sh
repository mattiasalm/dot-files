#!/usr/bin/env bash

printHeading "Hyper styling"
if [ -e "$HOME/.hyper.js" ]; then

    # Font size
    printInstalling "Font size"
    sed -i "" 's/fontSize.*,/fontSize: 14,/g' $HOME/.hyper.js
    printNoteSuccess "Done"

else
    printError "Hyper is missing"
fi